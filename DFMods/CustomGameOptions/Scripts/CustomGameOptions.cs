﻿using System;
using System.Text;
using System.Xml;
using System.IO;

public class CustomGameOptions
{
    private static bool ConfigsLoaded = false;
    private static string DataConfigPath = "Data/Config";
    private static string GameOptionsFilename = "CustomGameOptions.xml";
    public static bool ReloadUI = false;

    public static void LoadConfigs()
    {
        if (!ConfigsLoaded)
        {
            ConfigsLoaded = true;
            LoadGameOptions();
            Log.Out("Custom Game Options Loaded.");
        }
    }

    private static void LoadGameOptions()
    {
        var directory = GameIO.GetGameDir(DataConfigPath);
        if (File.Exists(directory + "/" + GameOptionsFilename))
        {
            XmlFile xmlFile = new XmlFile(GameOptionsFilename);
            XmlElement documentElement = xmlFile.XmlDoc.DocumentElement;
            if (documentElement.ChildNodes.Count == 0)
            {
                throw new Exception("No element <customgameoptions> found!");
            }

            foreach (object obj in documentElement.ChildNodes)
            {
                XmlNode xmlNode = (XmlNode)obj;
                if (xmlNode.NodeType == XmlNodeType.Element && xmlNode.Name.Equals("config"))
                {
                    XmlElement xmlElement = (XmlElement)xmlNode;

                    if (!xmlElement.HasAttribute("name"))
                    {
                        throw new Exception("Attribute 'name' missing on item");
                    }

                    if (!xmlElement.HasAttribute("value"))
                    {
                        throw new Exception("Attribute 'value' missing on item");
                    }

                    var value = xmlElement.GetAttribute("value");
                    switch (xmlElement.GetAttribute("name"))
                    {
                        case "SHZombiesMin":
                            if (int.TryParse(value, out GameOptions.SHZombiesMin))
                            {
                                if (GameOptions.SHZombiesMin > 60 || GameOptions.SHZombiesMin < 0)
                                    GameOptions.SHZombiesMin = GameOptions.SHZombiesMinDefault;
                            }
                            break;
                        case "SHZombiesMax":
                            if (int.TryParse(value, out GameOptions.SHZombiesMax))
                            {
                                if (GameOptions.SHZombiesMax > 60 || GameOptions.SHZombiesMax < 0)
                                    GameOptions.SHZombiesMax = GameOptions.SHZombiesMaxDefault;
                            }
                            break;
                        case "WHHoursMin":
                            if (int.TryParse(value, out GameOptions.WHHoursMin))
                            {
                                if (GameOptions.WHHoursMin > 100 || GameOptions.WHHoursMin < 0)
                                    GameOptions.WHHoursMin = GameOptions.WHHoursMinDefault;
                            }
                            break;
                        case "WHHoursMax":
                            if (int.TryParse(value, out GameOptions.WHHoursMax))
                            {
                                if (GameOptions.WHHoursMax > 100 || GameOptions.WHHoursMax < 0)
                                    GameOptions.WHHoursMax = GameOptions.WHHoursMaxDefault;
                            }
                            break;
                        case "WHZombiesMin":
                            if (int.TryParse(value, out GameOptions.WHZombiesMin))
                            {
                                if (GameOptions.WHZombiesMin > 60 || GameOptions.WHZombiesMin < 0)
                                    GameOptions.WHZombiesMin = GameOptions.WHZombiesMinDefault;
                            }
                            break;
                        case "WHZombiesMax":
                            if (int.TryParse(value, out GameOptions.WHZombiesMax))
                            {
                                if (GameOptions.WHZombiesMax > 60 || GameOptions.WHZombiesMax < 0)
                                    GameOptions.WHZombiesMax = GameOptions.WHZombiesMaxDefault;
                            }
                            break;
                    }
                }
            }
        }
        else
        {
            // This creates a new customgameoptions.xml with default values
            SaveGameOptions();
        }
    }

    public static void SaveGameOptions()
    {
        XmlDocument xmlDocument = new XmlDocument();
        XmlDeclaration newChild = xmlDocument.CreateXmlDeclaration("1.0", "UTF-8", null);
        xmlDocument.InsertBefore(newChild, xmlDocument.DocumentElement);
        XmlNode parent = xmlDocument.AppendChild(xmlDocument.CreateElement("customgameoptions"));

        parent.AppendChild(CreateXMLGameOptionChild(xmlDocument, "SHZombiesMin", GameOptions.SHZombiesMin.ToString()));
        parent.AppendChild(CreateXMLGameOptionChild(xmlDocument, "SHZombiesMax", GameOptions.SHZombiesMax.ToString()));
        parent.AppendChild(CreateXMLGameOptionChild(xmlDocument, "WHHoursMin", GameOptions.WHHoursMin.ToString()));
        parent.AppendChild(CreateXMLGameOptionChild(xmlDocument, "WHHoursMax", GameOptions.WHHoursMax.ToString()));
        parent.AppendChild(CreateXMLGameOptionChild(xmlDocument, "WHZombiesMin", GameOptions.WHZombiesMin.ToString()));
        parent.AppendChild(CreateXMLGameOptionChild(xmlDocument, "WHZombiesMax", GameOptions.WHZombiesMax.ToString()));

        Log.Out(SerializeToString(xmlDocument));

        var path = GameIO.GetGameDir(DataConfigPath) + "/" + GameOptionsFilename;
        File.WriteAllText(path, SerializeToString(xmlDocument), Encoding.UTF8);
    }

    private static XmlElement CreateXMLGameOptionChild(XmlDocument xmlDocument, string name, string value)
    {
        XmlElement xmlElement = xmlDocument.CreateElement("config");
        XmlAttribute xmlAttributeName = xmlDocument.CreateAttribute("name");
        xmlAttributeName.Value = name;
        xmlElement.Attributes.Append(xmlAttributeName);
        XmlAttribute xmlAttributeValue = xmlDocument.CreateAttribute("value");
        xmlAttributeValue.Value = value.ToLower();
        xmlElement.Attributes.Append(xmlAttributeValue);

        return xmlElement;
    }

    private static string SerializeToString(XmlDocument xmlDocument)
    {
        string result;
        using (StringWriter stringWriter = new StringWriter())
        {
            using (XmlTextWriter xmlTextWriter = new XmlTextWriter(stringWriter))
            {
                xmlDocument.WriteTo(xmlTextWriter);
                result = stringWriter.ToString();
            }
        }
        return result;
    }
}

public static class GameOptions
{
    static GameOptions()
    {
        CustomGameOptions.LoadConfigs();
    }

    public const int SHZombiesMinDefault = 15;
    public const int SHZombiesMaxDefault = 30;
    public const int WHHoursMinDefault = 24;
    public const int WHHoursMaxDefault = 30;
    public const int WHZombiesMinDefault = 10;
    public const int WHZombiesMaxDefault = 30;

    public static int SHZombiesMin = SHZombiesMinDefault;
    public static int SHZombiesMax = SHZombiesMaxDefault;
    public static int WHHoursMin = WHHoursMinDefault;
    public static int WHHoursMax = WHHoursMaxDefault;
    public static int WHZombiesMin = WHZombiesMinDefault;
    public static int WHZombiesMax = WHZombiesMaxDefault;
}
