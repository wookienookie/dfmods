﻿using HarmonyLib;
using System;
using System.Globalization;
using System.Reflection;
using UnityEngine;

public class CustomGameOptionsHarmony
{
    private const string CustomGamePrefSelectorId = "CustomGamePrefSelectorId";
    private const string CustomNamePrefix = "Custom";
    public static int GamePrefValue = 500; // We can no longer use LAST so need to set a manual number for this

    public class CustomGameOptionsHarmony_Init : IModApi
    {
        public void InitMod(Mod _modInstance)
        {
            Log.Out(" Loading Patch: " + this.GetType().ToString());

            // Reduce extra logging stuff
            Application.SetStackTraceLogType(UnityEngine.LogType.Log, StackTraceLogType.None);
            Application.SetStackTraceLogType(UnityEngine.LogType.Warning, StackTraceLogType.None);

            var harmony = new HarmonyLib.Harmony(GetType().ToString());
            harmony.PatchAll(Assembly.GetExecutingAssembly());
        }
    }

    [HarmonyPatch(typeof(XUiC_GamePrefSelector))]
    [HarmonyPatch("Init")]
    [HarmonyPatch(new Type[] { })]
    public class PatchXUiC_GamePrefSelectorInit
    {
        static bool Prefix(XUiC_GamePrefSelector __instance)
        {
            if (__instance.ViewComponent.ID.StartsWith(CustomNamePrefix))
            {
                // Store the ViewComonponent.ID in temp storage for use in Postfix and other Harmony methods below
                __instance.CustomAttributes.SetString(CustomGamePrefSelectorId, __instance.ViewComponent.ID);

                // Temporarily use enum 'Last'
                AccessTools.Field(typeof(XUiView), "id").SetValue(__instance.ViewComponent, "Last");
            }

            return true;
        }

        static void Postfix(XUiC_GamePrefSelector __instance,
            ref EnumGamePrefs ___gamePref)
        {
            // If we are a Custom GameOption then get the correct ViewComponent ID from temp storage, then set the correct ViewComponent.ID and the GamePref to a new custom Enum value
            var id = __instance.CustomAttributes.GetString(CustomGamePrefSelectorId);
            if (id.StartsWith(CustomNamePrefix))
            {
                GamePrefValue++;
                ___gamePref = (EnumGamePrefs)GamePrefValue;

                AccessTools.Field(typeof(XUiView), "id").SetValue(__instance.ViewComponent, id);
            }
        }
    }

    [HarmonyPatch(typeof(XUiC_GamePrefSelector))]
    [HarmonyPatch("SetVisible")]
    [HarmonyPatch(new Type[] { typeof(bool) })]
    public class PatchXUiC_GamePrefSelectorSetVisible
    {
        static bool Prefix(XUiC_GamePrefSelector __instance, ref bool _visible)
        {
            if (IsCustomGameOption(__instance))
            {
                __instance.ViewComponent.IsVisible = true;
                return false;
            }

            return true;
        }
    }

    [HarmonyPatch(typeof(XUiC_GamePrefSelector))]
    [HarmonyPatch("ControlCombo_OnValueChanged")]
    [HarmonyPatch(new Type[] { typeof(XUiController), typeof(XUiC_GamePrefSelector.GameOptionValue), typeof(XUiC_GamePrefSelector.GameOptionValue) })]
    public class PatchXUiC_GamePrefSelectorControlCombo_OnValueChanged
    {
        static bool Prefix(XUiC_GamePrefSelector __instance, XUiController _sender, XUiC_GamePrefSelector.GameOptionValue _oldValue, XUiC_GamePrefSelector.GameOptionValue _newValue)
        {
            if (IsCustomGameOption(__instance))
            {
                var id = __instance.CustomAttributes.GetString(CustomGamePrefSelectorId);

                switch (id)
                {
                    case "CustomSHZombiesMin":
                        GameOptions.SHZombiesMin = _newValue.IntValue;
                        break;
                    case "CustomSHZombiesMax":
                        GameOptions.SHZombiesMax = _newValue.IntValue;
                        break;
                    case "CustomWHHoursMin":
                        GameOptions.WHHoursMin = _newValue.IntValue;
                        break;
                    case "CustomWHHoursMax":
                        GameOptions.WHHoursMax = _newValue.IntValue;
                        break;
                    case "CustomWHZombiesMin":
                        GameOptions.WHZombiesMin = _newValue.IntValue;
                        break;
                    case "CustomWHZombiesMax":
                        GameOptions.WHZombiesMax = _newValue.IntValue;
                        break;
                }

                AccessTools.Method(typeof(XUiC_GamePrefSelector), "CheckDefaultValue").Invoke(__instance, new object[] { });
                return false;
            }

            return true;
        }
    }

    [HarmonyPatch(typeof(XUiC_GamePrefSelector))]
    [HarmonyPatch("SetCurrentValue")]
    [HarmonyPatch(new Type[] { })]
    public class PatchXUiC_GamePrefSelectorSetCurrentValue
    {
        static bool Prefix(XUiC_GamePrefSelector __instance, ref string[] ___valuesFromXml)
        {
            if (IsCustomGameOption(__instance))
            {
                var id = __instance.CustomAttributes.GetString(CustomGamePrefSelectorId);
                var controlCombo = AccessTools.Field(typeof(XUiC_GamePrefSelector), "controlCombo").GetValue(__instance) as XUiC_ComboBoxList<XUiC_GamePrefSelector.GameOptionValue>;
                var arrayValues = new int[___valuesFromXml.Length];
                for (int i = 0; i < ___valuesFromXml.Length; i++)
                {
                    arrayValues[i] = StringParsers.ParseSInt32(___valuesFromXml[i], 0, -1, NumberStyles.Integer);
                }

                switch (id)
                {
                    case "CustomSHZombiesMin":
                        controlCombo.SelectedIndex = Array.IndexOf(arrayValues, GameOptions.SHZombiesMin);
                        break;
                    case "CustomSHZombiesMax":
                        controlCombo.SelectedIndex = Array.IndexOf(arrayValues, GameOptions.SHZombiesMax);
                        break;
                    case "CustomWHHoursMin":
                        controlCombo.SelectedIndex = Array.IndexOf(arrayValues, GameOptions.WHHoursMin);
                        break;
                    case "CustomWHHoursMax":
                        controlCombo.SelectedIndex = Array.IndexOf(arrayValues, GameOptions.WHHoursMax);
                        break;
                    case "CustomWHZombiesMin":
                        controlCombo.SelectedIndex = Array.IndexOf(arrayValues, GameOptions.WHZombiesMin);
                        break;
                    case "CustomWHZombiesMax":
                        controlCombo.SelectedIndex = Array.IndexOf(arrayValues, GameOptions.WHZombiesMax);
                        break;
                }

                AccessTools.Method(typeof(XUiC_GamePrefSelector), "CheckDefaultValue").Invoke(__instance, new object[] { });
                return false;
            }

            return true;
        }
    }

    [HarmonyPatch(typeof(XUiC_GamePrefSelector))]
    [HarmonyPatch("IsDefaultValueForGameMode")]
    [HarmonyPatch(new Type[] { })]
    public class PatchXUiC_GamePrefSelectorIsDefaultValueForGameMode
    {
        static bool Prefix(XUiC_GamePrefSelector __instance, ref bool __result)
        {
            if (IsCustomGameOption(__instance))
            {
                var controlCombo = AccessTools.Field(typeof(XUiC_GamePrefSelector), "controlCombo").GetValue(__instance) as XUiC_ComboBoxList<XUiC_GamePrefSelector.GameOptionValue>;
                var id = __instance.CustomAttributes.GetString(CustomGamePrefSelectorId);

                switch (id)
                {
                    case "CustomSHZombiesMin":
                        __result = controlCombo.Value.IntValue == GameOptions.SHZombiesMinDefault;
                        break;
                    case "CustomSHZombiesMax":
                        __result = controlCombo.Value.IntValue == GameOptions.SHZombiesMaxDefault;
                        break;
                    case "CustomWHHoursMin":
                        __result = controlCombo.Value.IntValue == GameOptions.WHHoursMinDefault;
                        break;
                    case "CustomWHHoursMax":
                        __result = controlCombo.Value.IntValue == GameOptions.WHHoursMaxDefault;
                        break;
                    case "CustomWHZombiesMin":
                        __result = controlCombo.Value.IntValue == GameOptions.WHZombiesMinDefault;
                        break;
                    case "CustomWHZombiesMax":
                        __result = controlCombo.Value.IntValue == GameOptions.WHZombiesMaxDefault;
                        break;
                }

                return false;
            }

            return true;
        }
    }

    private static bool IsCustomGameOption(XUiC_GamePrefSelector __instance)
    {
        var id = __instance.CustomAttributes.GetString(CustomGamePrefSelectorId);

        return (!string.IsNullOrEmpty(id) && id.StartsWith(CustomNamePrefix));
    }

    [HarmonyPatch(typeof(XUiC_NewContinueGame))]
    [HarmonyPatch("SaveGameOptions")]
    [HarmonyPatch(new Type[] { })]
    public class PatchXUiC_NewContinueGameSaveGameOptions
    {
        static void Postfix(XUiC_NewContinueGame __instance)
        {
            Log.Out("SaveGameOptions");
            CustomGameOptions.SaveGameOptions();
        }
    }

    [HarmonyPatch(typeof(GameManager))]
    [HarmonyPatch("StartGame")]
    [HarmonyPatch(new Type[] { })]
    public class PatchGameManagerStartGame
    {
        static void Postfix(GameManager __instance)
        {
            // The Configs will automatically be loaded but we will still manually call LoadConfigs so that the CustomGameOptions.xml is generated on Dedicated servers without having to load into a world. The file should get created on server startup
            CustomGameOptions.LoadConfigs();
        }
    }
}

